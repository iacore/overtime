import std/[math, sugar]
import overtime, zero_functional

func osc_weird_pluck*(f, t: f32): f32 =
  # I got this as a bug
  let w = 2 * PI * f
  let wxt = w * t
  let exp_swxt = math.exp(-0.001 * wxt)
  let y0 = 0.6 * math.sin(wxt)
  let y1 = 0.2 * math.sin(2 * wxt)
  let y2 = 0.05 * math.sin(3 * wxt)
  let y3 = (y0 + y1 + y2) * exp_swxt
  let y4 = (1+y3)*y3*y3 # this line is different
  y4 * (1 + 16 * t * math.exp(-6 * t))

func osc_piano*(f, t: f32): f32 =
  ## Returns the intensity of a tone of frequency f sampled at time t
  ## t starts at 0 (note start)
  # https://dsp.stackexchange.com/questions/46598/mathematical-equation-for-the-sound-wave-that-a-piano-makes
  # https://youtu.be/ogFAHvYatWs?t=254
  # return int(2**13*(1+square(t, 440*2**(math.floor(5*t)/12))))
  # Y = sum([math.sin(2 * i * math.pi * t * f) * math.exp(-0.0004 * 2 * math.pi * t * f) / 2**i for i in range(1, 4)])
  # Y += Y * Y * Y
  # Y *= 1 + 16 * t * math.exp(-6 * t)
  let w = 2 * PI * f
  let ewt = math.exp(-0.001 * w * t)
  var Y = (
    0.6 * math.sin(w * t) +
    0.2 * math.sin(2 * w * t) +
    0.05 * math.sin(3 * w * t)
  ) * ewt
  
  let Y2 = Y * (Y * Y + 1)
  Y2 * (1 + 16 * t * math.exp(-6 * t))

func osc_pulse*(f, t: float, phasedrift: float = 0.0): float =
  let doublewidth = 1.0 / f
  let width = doublewidth / 2
  let phase: float = (t + doublewidth * phasedrift) mod doublewidth
  if phase < width: 1.0 else: -1.0

func osc_saw*(f,t:float, phasedrift: float = 0.0):float =
  let doublewidth = 1.0 / f
  let width = doublewidth / 2
  let phase: float = (t + doublewidth * phasedrift) mod doublewidth
  if phase < width:
    -1.0 + 2.0 * phase / width
  else:
    1.0 - 2.0 * (phase - width) / (1.0 - width)

func freq*(octave, step: float): float =
  ## Returns the frequency of a note
  55 * pow(2, (octave + step / 12 - 1))

type
  OscFn = proc(f, t: f32): f32
  FOsc = object
    m: ref Metronome
    f: OscFn
    freq: f32
    step_sec: f32
    ttl: f32
    time: f32
    gain: f32

const dummyOscFn: OscFn = (f, t: f32) => 0'f

proc newOsc(m: ref Metronome; f: OscFn): ref FOsc =
  (ref FOsc)(m: m, f: f, step_sec: 1 / m.srate, time: 0, ttl: 0, gain: 1)

proc isOn(fosc: ref FOsc): bool = fosc.time < fosc.ttl

proc tick(fosc: ref FOsc) =
  fosc.time += fosc.step_sec

proc sample(fosc: ref FOsc): f32 =
  if fosc.isOn:
    fosc.f(fosc.freq, fosc.time) * fosc.gain
  else:
    0'f

# clip length to 1 second
# osc = (f, t: float) => (if t > 1: 0.0 else: (osc_saw(f, t) * GAIN_NORMAL * 0.06) + (osc_pulse(f, t) * GAIN_NORMAL * 0.3))

# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

const DEFAULT_OSC = osc_piano

let m = newMetronome()
let master = m.newGain()
master.gain = 0.3

let li0 = m.newLimiter(master)
li0.threshold = 0.8; li0.multiplier = 0.5
let li  = m.newLimiter(li0)
li.threshold = 0.6; li.multiplier = 0.5

var vos_piano: array[8, ref FOsc]
for osc in mitems(vos_piano):
  osc = m.newOsc(DEFAULT_OSC)
  master &= osc
var active_vo_index = 0
proc nextVo(): auto =
  let osc = vos_piano[active_vo_index]
  active_vo_index = (active_vo_index + 1) mod vos_piano.len
  osc

const NOTE_UNIT_TIME = 0.23 # seconds per 1.0 note time
const OSC_DROPOFF = 1.0 # seconds

type Note = tuple
  pitch: int
  weight: f32
  dur: f32

proc playNote(note: Note) =
  let osc = nextVo()
  osc.time = 0'f
  osc.freq = 440.0 * pow(2.0, note.pitch / 12)
  osc.ttl = note.dur * NOTE_UNIT_TIME + OSC_DROPOFF
  osc.gain = note.weight

const R = -999
  ## rest

proc simpWeighted(melody: openArray[Note]) =
  for note in melody:
    let note = note
    playNote(note)
    m.wait Second(note.dur * NOTE_UNIT_TIME)

const mm = 1.0'f # regular
const p = 0.8'f # light
const pp = 0.7'f

proc hphph(a: int, b=a) =
  ## a is pitch
  ## h-ph-ph-
  simpWeighted([
    (a, mm, 2'f),
    (b, p, 1'f),
    (b, mm, 2'f),
    (b, p, 1'f),
    (b, mm, 2'f),
  ])

proc hhhr(a: int; b=a) =
  ## h-h-h-..
  simpWeighted([
    (a, mm, 2'f),
    (a, p, 2'f),
    (a, pp, 2'f),
    (b, mm, 2'f),
  ])

proc intro0 =
  hphph(0);hphph(0);hphph(7);hhhr(7,R)
  hphph(5,-2);hphph(-2);hphph(-5);hhhr(3)

template totaldur(s: openArray[Note]): f32 =
  s --> map(it.dur) --> sum()

let q0 = [
  (12, mm, 2'f).Note,
  (10, mm, 2),
  ( 5, mm, 2),
  ( 7, mm, 4),

  #
  ( 3, mm, 2),
  ( 5, mm, 2),
  ( 10,mm, 4),

  #
  ( 7, mm, 8),
  #
]
stderr.writeLine "what", q0.totaldur
# assert q0.totaldur == 32

let q1 = [
  (R, 0'f, 2'f).Note,
  (-2, mm, 4'f),
  (2,  mm, 10'f),
  (3,  mm, 2'f),
  (2,  mm, 2'f),
  (0,  mm, 2'f),
  (R, 0'f, 16'f),
]
assert q0.totaldur + q1.totaldur == 64, $(q0.totaldur + q1.totaldur)

proc main(m: ref Metronome) =
  intro0()
  intro0()

  # playNote((0, mm, 4'f))
  # playNote((3, mm, 4'f))
  # m.wait Second(4 * NOTE_UNIT_TIME)

  # main section

  simpWeighted(q0)
  simpWeighted(q1)

  simpWeighted(q0)
  simpWeighted(q1) # variant this?

var dac = li.toSource()
proc cb_tick(m: ref Metronome) =
  m.tick()
  let sample = dac.sample()
  let s = cast[array[4, uint8]](sample)
  discard stdout.writeBytes(s, 0, s.len)

m.spawn main
m.blockon cb_tick
