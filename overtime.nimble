# Package

version       = "0.1.0"
author        = "Locria Cyber"
description   = "sound over time"
license       = "AGPL-3.0-only"
srcDir        = "src"

# Dependencies

requires "nim >= 1.6"
requires "minicoro >= 0.1.3"
