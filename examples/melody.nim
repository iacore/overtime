# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

import overtime
import std/math

const R = -999

type Note = tuple
  pitch: int
  dur: f32

const melody = [
  (Note)((0, 1.0'f)),
  (2, 1.0'f),
  (3, 1.0'f),
  (0, 1.0'f),
  (7, 2.7'f),
  (R, 0.3'f),
  (7, 2.7'f),
  (R, 0.3'f),
  (5, 1.5'f),
  (R, 8.5'f),
  (5, 2.7'f),
  (R, 0.3'f),
  (5, 2.7'f),
  (R, 0.3'f),
  (3, 1.5'f),
  (R, 8.5'f),
]

let m = newMetronome()
let osc = m.newSinOsc()
let gain = newGain()
gain &= osc

proc main(m: ref Metronome) =
  for note in melody:
    if note.pitch == R:
      gain.gain = 0.0
    else:
      gain.gain = 1.0
      osc.freq = 440.0 * pow(2.0, note.pitch / 12)
    
    m.wait Second(note.dur * 0.2)

let dac = gain.toSource()
proc cb_tick(m: ref Metronome) =
  m.tick()
  let sample = dac.sample()
  let s = cast[array[4, uint8]](sample)
  discard stdout.writeBytes(s, 0, s.len)

m.spawn main
m.blockon cb_tick
