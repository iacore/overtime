overtime - sound over time

## Architecture

Chuck-like

The user is responsible for driving the DSP-Graph.

## Example

```
nelua tests/t_melody.nelua | play -r 44100 -t f32 -
```

`nelua` is from [nelua](https://nelua.io/).
`play` is from [SoX](https://sox.sourceforge.net/), he Swiss Army knife of sound processing programs.

## Install nelua

https://nelua.io/installing/

```
git clone https://github.com/edubart/nelua-lang.git && cd nelua-lang
make
```
