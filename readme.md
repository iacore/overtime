DSP system inspired by ChucK

Name: overtime
Name origin: amplitude over time



## Directory structure

- `./` Nim implementation
- `./nelua/` Nelua implementation

## Design Flaw: Metronome Rachet

Do not call `.sample()` on anything other than `Source`!

```
let m: ref Metronome = newMetronome()
let dac: Source = ...
m.tick()
dac.sample()
```
