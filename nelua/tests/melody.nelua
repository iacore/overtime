
require("io")
require("string")
require("metronome")
require("osc")

global Voice = @record{
    osc: *SinOsc,
    gain: *Gain,
}

function Voice.new(m: auto): Voice
    local osc = SinOsc.new(m)
    local gain = Gain.new(m)
    gain:add(osc)
    gain:set_gain(0.)
    return {
        osc,
        gain,
    }
end

function Voice:set_gain(x: auto)
    return self.gain:set_gain(x)
end

function Voice:set_freq(x: auto)
    return self.osc:set_freq(x)
end

global Note = @record {
    pitch: integer,
    len: float32,
}

global R = math.mininteger -- rest

local function hz12tet(base: float32, note_offset: integer)
    return base * (2. ^ (note_offset / 12))
end

global NotePlayer = @record {
    m: *Metronome,
    vo: *Voice,
    base_freq: float32,
}

function NotePlayer.new(m: *Metronome, vo: *Voice, base_freq: float32)
    return (@NotePlayer){
        m,
        vo,
        base_freq,
    }
end

function NotePlayer:play(note: Note)
    if note.pitch == R then
        self.vo:set_gain(0.)
    else
        self.vo:set_gain(1.)
        self.vo:set_freq(hz12tet(self.base_freq, note.pitch))
    end
    self.m:waitseconds(note.len)
end
