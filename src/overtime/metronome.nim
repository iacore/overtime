import minicoro
from std/strformat import `&`
import std/heapqueue
import std/sets
import util

type
  f32* = float32
  SampleTime* = distinct int
  Second* = distinct f32

# proc `+`(x: Second, y: Second): Second {.borrow.}
# proc `*`(x: Second, y: f32): Second {.borrow.}
proc `+`(x: SampleTime, y: SampleTime): SampleTime {.borrow.}
proc `-`(x: SampleTime, y: SampleTime): SampleTime {.borrow.}
proc `+=`(x: var SampleTime, y: int) {.borrow.}
# proc cmp(x: SampleTime, y: SampleTime): int {.borrow.}
proc `<`(x: SampleTime, y: SampleTime): bool {.borrow.}
proc `<=`(x: SampleTime, y: SampleTime): bool {.borrow.}
proc `$`*(x: SampleTime): string = &"SampleTime({x.int})"
# proc `*`(x: SampleTime, y: int): SampleTime {.borrow.}

type
  Shred = tuple
    wakeOn: SampleTime
    co: Coro

proc `<`(a, b: Shred): bool = a.wakeOn < b.wakeOn

type
  Metronome* = object
    time*: SampleTime
    srate*: f32
    pending: HeapQueue[Shred]
    has_ticked*: HashSet[uint]

proc `=destroy`*(m: var Metronome) =
  while m.pending.len > 0:
    let sh = m.pending.pop()
    sh.co.destroy()
  `=destroy`(m.pending)
  `=destroy`(m.has_ticked)

proc newMetronome*(): ref Metronome =
  new(result)
  result.time = SampleTime(0)
  result.srate = 44100.0

using m: ref Metronome

proc tick*(m) = m.has_ticked.clear()

proc run(m; co: Coro) =
  assert co.status == coSuspended
  co.resume()
  let status = co.status()
  case status
  of coSuspended:
    var wakeOn: SampleTime
    co.pop(wakeOn)
    m.pending.push((wakeOn, co))
  of coDead:
    co.destroy()
  else:
    raise newException(ValueError, &"Weird coroutine status: {status}")

proc wrapper(co: Coro) {.cdecl.} =
  var f: proc(m: ref Metronome)
  var m: ref Metronome
  co.pop(m)
  GC_unref(m)
  co.pop(f)
  assert co.bytesStored == 0
  co.yield()
  f(m)

proc spawn*(m; f: proc(m: ref Metronome)) =
  let desc = initCoroDesc(wrapper, 0)
  let co = desc.create()
  co.push(f)
  GC_ref(m)
  co.push(m)
  co.resume()
  m.pending.push((m.time, co))

proc waituntil*(m; instant: SampleTime) =
  let co: Coro = running()
  co.push(instant)
  co.yield()

proc wait*(m; dt: SampleTime) =
  m.waituntil(m.time + dt)

proc wait*(m; dt: Second) =
  m.waituntil(m.time + (dt.f32 * m.srate).SampleTime)

proc blockon*(m; cb_tick: proc(m: ref Metronome)) =
  while m.pending.len > 0:
    let sh = m.pending.pop()
    if sh.wakeOn >= m.time:
      for i in 0..<(sh.wakeOn-m.time).int:
        cb_tick(m)
        m.time += 1
    else:
      warn(&"shred wants to wake up in the past: {sh.wakeOn}")
    m.run(sh.co)
