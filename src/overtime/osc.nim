import std/[math, sets]
import metronome

type
  Source* = object
    ## osc vtable with built-in rachet (to ensure it is advanced only once per frame)
    id: uint # `osc.addr`
    m: ref Metronome
    m_tick: proc() ## call at begin of frame
    # m_tock: proc() ## call at end of frame
    m_sample: proc(): f32

proc toSource*(s: Source): Source = s

proc toSource*[T](osc: ref T): Source =
  mixin tick
  result.id = cast[uint](osc) # flaky (use memory address as id)
  assert osc.m != nil
  result.m = osc.m
  result.m_tick = proc() = osc.tick()
  result.m_sample = proc(): f32 = osc.sample()  

using vt: Source

proc flag_stuck(vt): bool = vt.id in vt.m.has_ticked

proc `flag_stuck=`(vt; v: bool) =
  if v:
    vt.m.has_ticked.incl vt.id
  else:
    vt.m.has_ticked.excl vt.id

proc sample*(vt): f32 =
  if not vt.flag_stuck:
    vt.flag_stuck = true
    vt.m_tick()
  # optimize: cache sample
  vt.m_sample()

type
  Gain* = object
    srcs*: seq[Source]
    gain*: f32
    m*: ref Metronome

using
  m: ref Metronome
  g: ref Gain

proc newGain*(m): ref Gain =
  new(result)
  result.m = m
  result.gain = 1.0

proc add*[T](g; osc: T) =
  g.srcs &= osc.toSource

proc tick*(g) = discard

proc sample*(g): f32 =
  for src in g.srcs:
    result += src.sample()
  result *= g.gain


import std/macros
macro mkSimpleOsc(ident): untyped =
  let ident_new = ident("new" & $ident)
  result = quote("@") do:
    type `@ident`* = object
      srate: f32
      phase*: f32
      step: f32
      m*: ref Metronome

    proc `freq=`*(osc: ref `@ident`; freq: f32) =
      var num: f32 = freq / osc.srate
      if num >= 1.0: num -= num.floor
      elif num <= -1.0: num += num.floor
      osc.step = num * (2 * PI)

    proc `@ident_new`*(m: ref Metronome): ref `@ident` =
      new(result)
      result.m = m
      result.srate = m.srate
      result.freq = 440.0
      result.phase = 0.0

    proc tick*(osc: ref `@ident`) =
      osc.phase += osc.step
      if osc.phase > PI:
        osc.phase -= 2*PI
      elif osc.phase < -PI:
        osc.phase += 2*PI

mkSimpleOsc(SinOsc)
proc sample*(osc: ref SinOsc): f32 = osc.phase.sin


type
  Limiter* = object
    ## Simple limiter
    inner*: Source
    multiplier*: f32
    threshold*: f32
    m*: ref Metronome

using li: ref Limiter

proc newLimiter*[T](m; inner: T): ref Limiter =
  new(result)
  result.m = inner.m
  when T is Source:
    result.inner = inner
  else:
    result.inner = inner.toSource
  result.threshold = 1'f
  result.multiplier = 0'f

proc tick*(li) = discard
proc sample*(li): f32 =
  var s = li.inner.sample()
  if s > li.threshold:
    s = li.threshold + (s - li.threshold) * li.multiplier
  elif s < -li.threshold:
    s = -li.threshold + (s - -li.threshold) * li.multiplier
  s
